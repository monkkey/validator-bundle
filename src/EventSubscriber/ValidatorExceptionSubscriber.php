<?php

namespace Monkkey\ValidatorBundle\EventSubscriber;

use Monkkey\ValidatorBundle\Exception\ValidatorException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class ValidatorExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['handleValidatorException', 0],
            ],
        ];
    }

    /**
     * @param GetResponseForExceptionEvent $event
     * @return void
     */
    public function handleValidatorException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof ValidatorException) {
            $data     = ["errors" => $exception->getErrors()];
            $status   = $exception->getStatusCode();
            $response = new JsonResponse($data, $status);
            $event->setResponse($response);
        }
    }
}
