<?php

namespace Monkkey\ValidatorBundle;

use Monkkey\ValidatorBundle\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;

final class Validator
{
    /**
     * @var ValidatorInterface
     */
    private $symfonyValidator;

    /**
     * @param ValidatorInterface $symfonyValidator
     */
    public function __construct(ValidatorInterface $symfonyValidator)
    {
        $this->symfonyValidator = $symfonyValidator;
    }

    /**
     * Throws a ValidatorException if the validation returned some errors
     *
     * @param  mixed                                              $data
     * @param  int                                                $statusCode
     * @param  Constraint|Constraint[]|null                       $constraints
     * @param  string|GroupSequence|(string|GroupSequence)[]|null $groups
     * @return void
     */
    public function validate(
        $data,
        int $statusCode = Response::HTTP_BAD_REQUEST,
        array $constraints = null,
        array $groups = null
    ): void {
        /** @var ConstraintViolationList $violations */
        $violations = $this->symfonyValidator->validate(
            $data,
            $constraints,
            $groups
        );
  
        if ($violations->count()) {
            $errors = [];
            // Stack the errors
            foreach ($violations as $violation) {
                $errors[] = [
                    "subject" => preg_replace(["/\[/", "/\]/"], "", $violation->getPropertyPath()),
                    "message" => $violation->getMessage(),
                ];
            }
            // Throw the exception with the violations errors
            throw new ValidatorException($errors, $statusCode);
        }
    }
}
