<?php

namespace Monkkey\ValidatorBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

final class ValidatorException extends HttpException
{
    /**
     * @var array
     */
    private $errors;

    public function __construct(
        array $errors,
        int $statusCode = 400,
        string $message = "Invalid request.",
        array $headers = []
    ) {
        parent::__construct($statusCode, $message, null, $headers, 0);
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
