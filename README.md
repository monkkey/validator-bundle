# Validator Bundle

## Requirements

* PHP 7.1
* Composer 1.7

## Installation

Run the following command

```shell
composer require monkkey/validator-bundle
```
